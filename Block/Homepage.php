<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Variable\Model\Variable;
use Magento\Variable\Model\VariableFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Rbm\Test\Setup\InstallData;

class Homepage extends \Magento\Framework\View\Element\Template
{
    /**
     * @var VariableFactory
     */
    private $variableFactory;
    /**
     * @var Json
     */
    private $json;

    /**
     * @var InstallData
     */
    private $installData;

    /**
     * Homepage constructor.
     *
     * @param Context $context
     * @param VariableFactory $variableFactory
     * @param Json $json
     * @param InstallData $installData
     * @param array $data
     */
    public function __construct(
        Context $context,
        VariableFactory $variableFactory,
        Json $json,
        InstallData $installData,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->variableFactory = $variableFactory;
        $this->json = $json;
        $this->installData = $installData;
    }

    /**
     * Return the Url for saving.
     *
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->_urlBuilder->getUrl(
            'rbm_test/user/create'
        );
    }

    /**
     * Get countries array
     *
     * @return array
     */
    public function getCountries()
    {
        $variable = $this->variableFactory->create();
        $variable->loadByCode(InstallData::COUNTRY_LIST_VARIABLE);

        if ($variable->getCode() !== InstallData::COUNTRY_LIST_VARIABLE) {
            $variable = $this->installData->saveCountryList();
        }

        return $this->json->unserialize($variable->getValue(Variable::TYPE_TEXT));
    }

    /**
     * Get Countries Option Array
     *
     * @return \string[][]
     */
    private function getCountriesOptionArray(): array
    {
        $options = [
            ['value' => '', 'label' => 'Country:'],
        ];
        foreach ($this->getCountries() as $i => $country) {
            $options[] = [
                'label' => current($country),
                'value' => key($country)
            ];
        }

        return $options;
    }

    /**
     * Get country by code
     *
     * @param string $code
     * @return string
     */
    public function getCountryByCode(string $code): string
    {
        foreach ($this->getCountries() as $country) {
            if ($code === ltrim((string)key($country), '0')) {
                return current($country);
            }
        }

        return $code;
    }

    /**
     * @inheritDoc
     */
    protected function _beforeToHtml()
    {
        $this->jsLayout['components']['user']['children']['user-form-fieldset']['children']['country']
        ['config']['options'] = $this->getCountriesOptionArray();

        return parent::_beforeToHtml();
    }
}
