# Configuration
app/etc/env.php
should be added DB connection:

```
    'db' => [
        'table_prefix' => '',
        'connection' => [
            'default' => [
                'host' => 'database',
                'dbname' => 'main_db',
                'username' => 'root',
                'password' => 'root',
                'active' => '1'
            ],
            'rbm_test' => [
                'host' => 'database',
                'dbname' => 'rbm_test',
                'username' => 'root',
                'password' => 'root',
                'active' => '1'
            ]
        ]
    ],
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ],
        'rbm_test' => [
            'connection' => 'rbm_test'
        ]
    ],

```

I used install scripts instead of the declarative schema because I couldn't find a way to create tables in another database using the declarative schema.

I would be grateful if you could clarify this point
