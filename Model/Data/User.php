<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Model\Data;

use Rbm\Test\Api\Data\UserInterface;

/**
 * Class User
 */
class User extends \Magento\Framework\Api\AbstractExtensibleObject implements UserInterface
{
    /**
     * Get user_id
     * @return string|null
     */
    public function getUserId(): ?string
    {
        return $this->_get(self::USER_ID);
    }

    /**
     * Set user_id
     * @param string $userId
     * @return \Rbm\Test\Api\Data\UserInterface
     */
    public function setUserId(string $userId): \Rbm\Test\Api\Data\UserInterface
    {
        return $this->setData(self::USER_ID, $userId);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \Rbm\Test\Api\Data\UserInterface
     */
    public function setName(string $name): \Rbm\Test\Api\Data\UserInterface
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get email
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->_get(self::EMAIL);
    }

    /**
     * Set email
     * @param string $email
     * @return \Rbm\Test\Api\Data\UserInterface
     */
    public function setEmail(string $email): \Rbm\Test\Api\Data\UserInterface
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * Get Phone
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->_get(self::PHONE);
    }

    /**
     * Set phone
     * @param string $phone
     * @return \Rbm\Test\Api\Data\UserInterface
     */
    public function setPhone(string $phone): \Rbm\Test\Api\Data\UserInterface
    {
        return $this->setData(self::PHONE, $phone);
    }

    /**
     * Get Address
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->_get(self::ADDRESS);
    }

    /**
     * Set Address
     * @param string $address
     * @return \Rbm\Test\Api\Data\UserInterface
     */
    public function setAddress(string $address): \Rbm\Test\Api\Data\UserInterface
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * Get Country
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->_get(self::COUNTRY);
    }

    /**
     * Set country
     * @param string $country
     * @return \Rbm\Test\Api\Data\UserInterface
     */
    public function setCountry(string $country): \Rbm\Test\Api\Data\UserInterface
    {
        return $this->setData(self::COUNTRY, $country);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Rbm\Test\Api\Data\UserExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Rbm\Test\Api\Data\UserExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Rbm\Test\Api\Data\UserExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}

