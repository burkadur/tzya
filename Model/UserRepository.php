<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Rbm\Test\Api\Data\UserInterfaceFactory;
use Rbm\Test\Api\Data\UserSearchResultsInterfaceFactory;
use Rbm\Test\Api\UserRepositoryInterface;
use Rbm\Test\Model\ResourceModel\User as ResourceUser;
use Rbm\Test\Model\ResourceModel\User\CollectionFactory as UserCollectionFactory;

/**
 * Class UserRepository
 */
class UserRepository implements UserRepositoryInterface
{

    protected $searchResultsFactory;

    protected $userFactory;

    protected $userCollectionFactory;

    private $storeManager;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $dataObjectHelper;

    protected $dataUserFactory;

    protected $extensibleDataObjectConverter;
    protected $resource;


    /**
     * @param ResourceUser $resource
     * @param UserFactory $userFactory
     * @param UserInterfaceFactory $dataUserFactory
     * @param UserCollectionFactory $userCollectionFactory
     * @param UserSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceUser $resource,
        UserFactory $userFactory,
        UserInterfaceFactory $dataUserFactory,
        UserCollectionFactory $userCollectionFactory,
        UserSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->userFactory = $userFactory;
        $this->userCollectionFactory = $userCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataUserFactory = $dataUserFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Rbm\Test\Api\Data\UserInterface $user): \Rbm\Test\Api\Data\UserInterface
    {


        $userData = $this->extensibleDataObjectConverter->toNestedArray(
            $user,
            [],
            \Rbm\Test\Api\Data\UserInterface::class
        );

        $userModel = $this->userFactory->create()->setData($userData);

        try {
            $this->resource->save($userModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the user: %1',
                $exception->getMessage()
            ));
        }
        return $userModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $userId): \Rbm\Test\Api\Data\UserInterface
    {
        $user = $this->userFactory->create();
        $this->resource->load($user, $userId);
        if (!$user->getId()) {
            throw new NoSuchEntityException(__('User with id "%1" does not exist.', $userId));
        }
        return $user->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->userCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Rbm\Test\Api\Data\UserInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(\Rbm\Test\Api\Data\UserInterface $user)
    {
        try {
            $userModel = $this->userFactory->create();
            $this->resource->load($userModel, $user->getUserId());
            $this->resource->delete($userModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the User: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($userId)
    {
        return $this->delete($this->get($userId));
    }
}

