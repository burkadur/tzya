<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Model;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\HTTP\AsyncClient\HttpResponseDeferredInterface;
use Magento\Framework\HTTP\AsyncClient\Request;
use Magento\Framework\HTTP\AsyncClient\ResponseFactory;
use Magento\Framework\HTTP\AsyncClientInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Psr\Log\LoggerInterface;

/**
 * Class Adapter
 */
class Adapter
{
    /**
     * @var AsyncClientInterface
     */
    private $httpClient;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Adapter constructor.
     * @param AsyncClientInterface $httpClient
     * @param Json $json
     * @param LoggerInterface $logger
     */
    public function __construct(
        AsyncClientInterface $httpClient,
        Json $json,
        LoggerInterface $logger
    ) {
        $this->httpClient = $httpClient;
        $this->json = $json;
        $this->logger = $logger;
    }

    /**
     *
     *
     * @return string
     * @throws LocalizedException
     */
    public function request(): string
    {
        $responseBody = '';

        try {
            $response = $this->httpClient->request(
                new Request(
                    $this->getGatewayURL(),
                    Request::METHOD_GET,
                    ['Content-Type' => 'application/json'],
                    null
                )
            );

            if ($response->get()->getStatusCode() === 200) {
                $responseBody = $response->get()->getBody();
            }

            $level = $response->get()->getStatusCode() === 200 ? 'info' : 'warning';
            $this->logger->$level('Response status code:' . $response->get()->getStatusCode());
            $this->logger->$level('Headers:' . $this->json->serialize($response->get()->getHeaders()));
            $this->logger->$level('Body:' . $this->json->serialize($response->get()->getBody()));

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            $this->logger->error($e->getTraceAsString());
        }

        return $responseBody;
    }

    private function getGatewayURL()
    {
        return 'https://restcountries.eu/rest/v2/all';
    }
}
