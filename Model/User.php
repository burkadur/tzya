<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Model;

use Magento\Framework\Api\DataObjectHelper;
use Rbm\Test\Api\Data\UserInterface;
use Rbm\Test\Api\Data\UserInterfaceFactory;

/**
 * Class User
 */
class User extends \Magento\Framework\Model\AbstractModel
{
    protected $_eventPrefix = 'rbm_test_user';
    protected $dataObjectHelper;
    protected $userDataFactory;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param UserInterfaceFactory $userDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Rbm\Test\Model\ResourceModel\User $resource
     * @param \Rbm\Test\Model\ResourceModel\User\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        UserInterfaceFactory $userDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Rbm\Test\Model\ResourceModel\User $resource,
        \Rbm\Test\Model\ResourceModel\User\Collection $resourceCollection,
        array $data = []
    ) {
        $this->userDataFactory = $userDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve user model with user data
     * @return UserInterface
     */
    public function getDataModel()
    {
        $userData = $this->getData();

        $userDataObject = $this->userDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $userDataObject,
            $userData,
            UserInterface::class
        );

        return $userDataObject;
    }
}

