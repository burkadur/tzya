<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Model;

use Rbm\Test\Api\Data\UserInterface;
use Rbm\Test\Api\FormValidatorInterface;

/**
 * Class FormValidator
 */
class FormValidator implements FormValidatorInterface
{

    /**
     * @var array
     */
    private $rules = [
        UserInterface::NAME => [
            'NotEmpty'
        ],
        UserInterface::PHONE => [
            'NotEmpty'
        ],
        UserInterface::EMAIL => [
            'NotEmpty',
            'EmailAddress'
        ],
        UserInterface::ADDRESS => [
            'NotEmpty'
        ],
    ];

    /**
     * Validate data
     *
     * @param array $data
     * @return bool
     * @throws \Zend_Validate_Exception
     */
    public function execute(array $data): bool
    {
        foreach ($data as $field => $value) {
            if (isset($this->rules[$field])) {
                foreach ($this->rules[$field] as $rule) {
                    if (!\Zend_Validate::is(trim($value), $rule)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
