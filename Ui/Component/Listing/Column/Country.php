<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Ui\Component\Listing\Column;

use Rbm\Test\Api\Data\UserInterface;
use Rbm\Test\Block\Homepage;

/**
 * Class Country
 */
class Country extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @var Homepage
     */
    private $homepageHelper;

    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param Homepage $homepageHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        Homepage $homepageHelper,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->homepageHelper = $homepageHelper;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item[UserInterface::COUNTRY])) {
                    $item[UserInterface::COUNTRY] = $this->homepageHelper->getCountryByCode($item[UserInterface::COUNTRY]);
                }
            }
        }

        return $dataSource;
    }
}

