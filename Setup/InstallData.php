<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Rbm\Test\Model\Adapter;
use Magento\Variable\Model\VariableFactory;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class InstallData
 */
class InstallData implements InstallDataInterface
{
    const COUNTRY_LIST_VARIABLE = 'test_country_list';

    /**
     * @var Adapter
     */
    private $adapter;

    /**
     * @var VariableFactory
     */
    private $variableFactory;

    /**
     * @var Json
     */
    private $json;

    /**
     * InstallData constructor.
     * @param Adapter $adapter
     * @param VariableFactory $variableFactory
     * @param Json $json
     */
    public function __construct(
        Adapter $adapter,
        VariableFactory $variableFactory,
        Json $json
    ) {
        $this->adapter = $adapter;
        $this->variableFactory = $variableFactory;
        $this->json = $json;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->saveCountryList();

        $setup->endSetup();
    }

    /**
     * Get a list of countries from the remote API and save it to a custom variable
     *
     * @return \Magento\Variable\Model\Variable
     */
    public function saveCountryList()
    {
        $jsonData = $this->adapter->request();
        $countries = $this->json->unserialize($jsonData);
        $countries = array_map(static function (array $country) {
            $code = $country['numericCode'] ?? $country['alpha3Code'];
            return [$code => $country['name']];
        }, $countries);

        /** @var \Magento\Variable\Model\Variable $variable */
        $variable = $this->variableFactory->create();
        $data = [
            'code' => self::COUNTRY_LIST_VARIABLE,
            'name' => self::COUNTRY_LIST_VARIABLE,
            'html_value' => '',
            'plain_value' => $this->json->serialize($countries),

        ];
        $variable->setData($data);
        $variable->save();

        return $variable;
    }
}
