<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $table = $setup->getConnection('rbm_test')
            ->newTable($setup->getTable('rbm_test_user'))
            ->addColumn(
                'user_id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )->addColumn(
                'name',
                Table::TYPE_TEXT,
                100,
                [],
                'Name'
            )->addColumn(
                'email',
                Table::TYPE_TEXT,
                100,
                ['nullable' => false, 'comment' => 'Email'],
                'Email'
            )->addColumn(
                'phone',
                Table::TYPE_TEXT,
                100,
                [],
                'phone'
            )->addColumn(
                'address',
                Table::TYPE_TEXT,
                300,
                [],
                'address'
            )->addColumn(
                'country',
                Table::TYPE_SMALLINT,
                null,
                ['unsigned' => true, 'nullable' => false],
                'country'
            );
        $setup->getConnection('rbm_test')->createTable($table);
    }
}
