/**
 * Copyright © Borys Ruvinskyi. All rights reserved.
 * See LICENSE_MONSOON.txt for license details
 */
define(
    [
        'Magento_Ui/js/form/form',
        'Rbm_Test/js/action/submitUser'
    ],
    function (Component, submitUser) {
        'use strict';

        return Component.extend({

            /**
             * @returns {exports}
             */
            initialize: function () {
                this._super();

                return this;
            },

            /**
             * Form submit handler
             *
             * This method can have any name.
             */
            onSubmit: function () {
                this.source.set('params.invalid', false);
                this.source.trigger('userForm.data.validate');

                if (!this.source.get('params.invalid')) {
                    submitUser(this, function () {
                        }.bind(this)
                    );
                }
            },
        });
    }
);
