/**
 * Copyright © Borys Ruvinskyi. All rights reserved.
 * See LICENSE_MONSOON.txt for license details
 */
define([
    'jquery',
    'mage/storage',
    'mage/translate'
], function ($, storage, $t) {
    'use strict';

    /**
     * @param {Object} userForm
     * @param {Object} messageContainer
     */
    return function (userForm, callback) {

        return storage.post(
            userForm.action,
            JSON.stringify(userForm.source.get('userForm'))
        ).done(function (response) {
            console.log(response);
            userForm.reset();
        }).fail(function () {
            alert($t('An error has occurred. Please try again later'));
        });
    };
});
