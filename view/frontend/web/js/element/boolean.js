/**
 * Copyright © Borys Ruvinskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'Magento_Ui/js/form/element/boolean'
], function (Component) {
    'use strict';

    return Component.extend({
        defaults: {
            checked: !!Math.floor(Math.random() * Math.floor(2)),
            links: {
                checked: 'value'
            }
        },

        /**
         * @returns {*|void|Element}
         */
        initObservable: function () {
            return this._super()
                    .observe('checked');
        },

        /**
         * Converts provided value to boolean.
         *
         * @returns {Boolean}
         */
        normalizeData: function () {
            return !!+this._super();
        },

        /**
         * Calls 'onUpdate' method of parent, if value is defined and instance's
         *     'unique' property set to true, calls 'setUnique' method
         *
         * @return {Object} - reference to instance
         */
        onUpdate: function () {
            if (this.hasUnique) {
                this.setUnique();
            }

            return this._super();
        }
    });
});
