<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface UserRepositoryInterface
{
    /**
     * Save User
     * @param \Rbm\Test\Api\Data\UserInterface $user
     * @return \Rbm\Test\Api\Data\UserInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Rbm\Test\Api\Data\UserInterface $user): \Rbm\Test\Api\Data\UserInterface;

    /**
     * Retrieve User
     * @param string $userId
     * @return \Rbm\Test\Api\Data\UserInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get(string $userId): \Rbm\Test\Api\Data\UserInterface;

    /**
     * Retrieve User matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Rbm\Test\Api\Data\UserSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete User
     * @param \Rbm\Test\Api\Data\UserInterface $user
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Rbm\Test\Api\Data\UserInterface $user);

    /**
     * Delete User by ID
     * @param string $userId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($userId);
}

