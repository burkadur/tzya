<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Api;

/**
 * Interface FormValidatorInterface
 */
interface FormValidatorInterface
{

    /**
     * Validate fields according to components from jsLayout form
     *
     * @param array $data
     * @return bool
     */
    public function execute(array $data): bool;
}
