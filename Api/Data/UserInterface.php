<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Api\Data;

interface UserInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const USER_ID = 'user_id';
    const NAME = 'name';
    const EMAIL = 'email';
    const PHONE = 'phone';
    const ADDRESS = 'address';
    const COUNTRY = 'country';

    /**
     * Get user_id
     * @return string|null
     */
    public function getUserId(): ?string;

    /**
     * Set user_id
     * @param string $userId
     * @return \Rbm\Test\Api\Data\UserInterface
     */
    public function setUserId(string $userId): \Rbm\Test\Api\Data\UserInterface;

    /**
     * Get name
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * Set name
     * @param string $name
     * @return \Rbm\Test\Api\Data\UserInterface
     */
    public function setName(string $name): \Rbm\Test\Api\Data\UserInterface;

    /**
     * Get email
     * @return string|null
     */
    public function getEmail(): ?string;

    /**
     * Set email
     * @param string $email
     * @return \Rbm\Test\Api\Data\UserInterface
     */
    public function setEmail(string $email): \Rbm\Test\Api\Data\UserInterface;

    /**
     * Get phone
     * @return string|null
     */
    public function getPhone(): ?string;

    /**
     * Set phone
     * @param string $phone
     * @return \Rbm\Test\Api\Data\UserInterface
     */
    public function setPhone(string $phone): \Rbm\Test\Api\Data\UserInterface;

    /**
     * Get Address
     * @return string|null
     */
    public function getAddress(): ?string;

    /**
     * Set address
     * @param string $address
     * @return \Rbm\Test\Api\Data\UserInterface
     */
    public function setAddress(string $address): \Rbm\Test\Api\Data\UserInterface;

    /**
     * Get Country
     * @return string|null
     */
    public function getCountry(): ?string;

    /**
     * Set country
     * @param string $country
     * @return \Rbm\Test\Api\Data\UserInterface
     */
    public function setCountry(string $country): \Rbm\Test\Api\Data\UserInterface;

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Rbm\Test\Api\Data\UserExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Rbm\Test\Api\Data\UserExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Rbm\Test\Api\Data\UserExtensionInterface $extensionAttributes
    );
}

