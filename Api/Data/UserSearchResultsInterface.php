<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Api\Data;

/**
 * Interface UserSearchResultsInterface
 */
interface UserSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get User list.
     * @return \Rbm\Test\Api\Data\UserInterface[]
     */
    public function getItems();

    /**
     * Set email list.
     * @param \Rbm\Test\Api\Data\UserInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

