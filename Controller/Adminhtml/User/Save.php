<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Controller\Adminhtml\User;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save
 */
class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('user_id');

            $model = $this->_objectManager->create(\Rbm\Test\Model\User::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This User no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the User.'));
                $this->dataPersistor->clear('rbm_test_user');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['user_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the User.'));
            }

            $this->dataPersistor->set('rbm_test_user', $data);
            return $resultRedirect->setPath('*/*/edit', ['user_id' => $this->getRequest()->getParam('user_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}

