<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Controller\Adminhtml\User;

/**
 * Class Delete
 */
class Delete extends \Rbm\Test\Controller\Adminhtml\User
{
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('user_id');
        if ($id) {
            try {
                $model = $this->_objectManager->create(\Rbm\Test\Model\User::class);
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the User.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['user_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a User to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}

