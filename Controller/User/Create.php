<?php
/**
 * Copyright © Borys Ruvinskyi All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Rbm\Test\Controller\User;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Exception\LocalizedException;
use Rbm\Test\Api\UserRepositoryInterface;
use Rbm\Test\Model\Data\UserFactory;
use Rbm\Test\Api\FormValidatorInterface;
use Magento\Framework\Serialize\Serializer\Json as JsonHelper;

/**
 * Class Create
 */
class Create extends Action
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * @var FormValidatorInterface
     */
    private $formValidator;
    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * Create constructor.
     * @param Context $context
     * @param UserRepositoryInterface $userRepository
     * @param UserFactory $userFactory
     * @param FormValidatorInterface $formValidator
     * @param JsonHelper $jsonHelper
     */
    public function __construct(
        Context $context,
        UserRepositoryInterface $userRepository,
        UserFactory $userFactory,
        FormValidatorInterface $formValidator,
        JsonHelper $jsonHelper
    ) {
        parent::__construct($context);
        $this->userRepository = $userRepository;
        $this->userFactory = $userFactory;
        $this->formValidator = $formValidator;
        $this->jsonHelper = $jsonHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        try {
            if ($this->getRequest()->isXmlHttpRequest()) {
                $content = $this->getRequest()->getContent();
                $data = $this->jsonHelper->unserialize($content);

                if (!$this->formValidator->execute($data)) {
                    throw new LocalizedException('The data is not valid');
                }
                /** @var \Rbm\Test\Api\Data\UserInterface $user */
                $user = $this->userFactory->create();
                $user->setName($data['name'])
                    ->setEmail($data['email'])
                    ->setPhone($data['phone'])
                    ->setAddress($data['address'])
                    ->setCountry($data['country']);
                $this->userRepository->save($user);

                $responseContent = ['success' => true, 'error_message' => ''];
            }

        } catch (LocalizedException $e) {
            $responseContent = ['success' => false, 'error_message' => $e->getMessage()];
        } catch (\Exception $e) {
            $responseContent = [
                'success' => false,
                'error_message' => __('We can\'t save this new user right now.')
            ];
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($responseContent);
        return $resultJson;
    }


}
